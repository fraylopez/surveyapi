package src.mogodev.util;
import haxe.Json;

/**
 * ...
 * @author mogodev
 */
class Helper
{

	public static function parseJson(path:String) : Dynamic
	{
		var value = sys.io.File.getContent(path),
        json = haxe.Json.parse(value);
		return json;
	}
	
	public static function flattenObject(ob:Dynamic):Dynamic 
	{
		if (Type.typeof(ob) != Type.ValueType.TObject) return ob;
		
		var toReturn = {};
		for (i in Reflect.fields(ob)) {
			if (!Reflect.hasField(ob, i)) continue;
			if (Type.typeof(Reflect.field(ob,i)) == Type.ValueType.TObject) {
				var flatObject = flattenObject(Reflect.field(ob,i));
				for (x in Reflect.fields(flatObject)) {
					if (!Reflect.hasField(flatObject, x)) continue;
					Reflect.setField(toReturn, i + '.' + x, Reflect.field(flatObject,x));
				}
			} else {
				Reflect.setField(toReturn, i, Reflect.field(ob, i));
			}
		}
		return toReturn;
	}
	
	public static function intersectByField<T>(arr1: Array<T>, arr2: Array<T>, f:T->Dynamic, field: String): Array<T> {
		
		return Lambda.array(Lambda.filter(arr1, 
			function (a:T):Bool 
			{
				return Lambda.exists(arr2, 
					function (b:T):Bool 
					{
						return return Reflect.hasField(f(a), field) && Reflect.hasField(f(b), field) && Reflect.field(f(a), field) == Reflect.field(f(b), field);
					});
			}));

	}
	
}