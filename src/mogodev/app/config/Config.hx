package mogodev.app.config;
import js.Node;
import js.node.Path;

/**
 * ...
 * @author mogodev
 */
class Config
{
	static public inline var URL:String = "http://localhost";
	static public inline var PORT:Int = 9998;
	
	static public inline var ENDPOINT:String = URL +":" + PORT;
	static public inline var API_ENDPOINT:String = ENDPOINT + "/api";
	static public inline var TEST_ENDPOINT:String = ENDPOINT + "/test";
	static public inline var TEST_REACT:String = ENDPOINT + "/test-react";
	
	static public var CLIENT_PATH:String = Path.join(Node.__dirname , '..', '..', 'client');
	static public var CLIENT_REACT_PATH:String = Path.join(Node.__dirname , '..', '..', 'client-react');
	static public var DATA_PATH:String = Path.join(Node.__dirname , '..', 'data', 'sampleData.json');
	
}