package src.mogodev.app.server.routes;
import abe.IRoute;
import haxe.Json;
import js.Error;
import jsonSchema.JsonSchemaGenerator;
import mogodev.app.model.NodeOption;
import mogodev.app.model.SurveyRequest.Country;
import mogodev.app.model.SurveyRequest.Subscription;
import mogodev.app.model.SurveyRequest.Survey;
import mogodev.app.server.search.SearchEngine;
import src.mogodev.app.server.routes.RouteHandler;

/**
 * ...
 * @author mogodev
 * 
 * API self defines while navigating nodes
 * a "callable" node will send its data model as json schema
 * otherwise offers NodeOptions
 * NodeOption contains information about the node
*/

typedef AppProperties = {
	clientUrl:String
}

@:path("/api")
class RouteHandler implements IRoute
{
	var searchEngine:SearchEngine;
		
	public function new(searchEngine:SearchEngine):Void 
	{
		this.searchEngine = searchEngine;
	}
	
	@:get("/")
	function getOptions () {
			
		var result:Array<NodeOption> = 
			[
				new NodeOption(
					"Single Request",
					"single",
					"Explore surveys based on single known parameters."
				),
				new NodeOption(
					"Composed Request",
					"request",
					"Compose a request using several search criteria."
				)
			];
		response.send(result);
	}
	
	@:get("/single/")
	function getSurveyOptions () {
					
		var result:Array<NodeOption> = 
			[
				new NodeOption(
					"By Id",
					"id",
					"Find surveys by id (i.e. integer > 0).",
					JsonSchemaGenerator.generate(Int)
				),
				new NodeOption(
					"By Key Word",
					"search",
					"Find surveys using related topics(i.e 'lorem','impsum','dolor').",
					JsonSchemaGenerator.generate(String)
				),
				new NodeOption(
					"By Country",
					"country",
					"Find surveys conducted at a country ('France', 'Spain', 'Congo', 'Vatican',...)",
					JsonSchemaGenerator.generate(Country)
				)
			];
			
		response.send(result);
	}

	@:get("/request/")
	function getRequestOptions () {
					
		var result:Array<NodeOption> = 
			[
				new NodeOption(
					"Search",
					"survey",
					"Find surveys matching several search criteria",
					JsonSchemaGenerator.generate(Survey)
				),
				new NodeOption(
					"Subscribe",
					"subscription",
					"Subscribe to this service",
					JsonSchemaGenerator.generate(Subscription)
				)
			];
			
		response.send(result);
	}
	
	//Single Filters
	@:get("/single/id/:id")
	function findById(id : String) {
		var result = this.searchEngine.searchByField("id", id);
		response.send(result);
	}
	
	//Search by key words
	@:get("/single/search/:tag")
	function getSurveysByTag(tag : String) {
		var result = this.searchEngine.searchByField("tags", tag);
		response.send(result);
	}
	
	@:get("/single/country/:id")
	function findByCountry(id : String) {
		var result = this.searchEngine.searchByField("country", id);
		response.send(result);
	}
	
	//Composed requests
	@:get("/request/survey/:survey")
	function searchSurvey(survey: String) {
		var s = Json.parse(survey);
		var result = this.searchEngine.searchObject(s);
			
		response.send(result);
	}
	
	@:get("/request/subscription/:subscription")
	function subscriptionHanlder(subscription: String) {
		var s = Json.parse(subscription);
		var demoMsg = "";
		if (s.frequency == null || s.channel == null) {
			demoMsg = "You should setup a frequency and a channel...";
		}
		else 
		{
			demoMsg = "You've been added to our subsctiption service.";
			demoMsg += " You'll receive the info " + s.frequency;
			
			var last = s.channel.pop();
			demoMsg += " through " + (s.channel.length > 0 ? (s.channel.join(", ") + " and " + last) : last);
		}
		response.send(demoMsg);
	}
	
	//not found fallback
	@:get("*")
	function notFoundFallback () {
				
		var errorName:String = "Error";
		var errorMsg:String = "Not found";

		var error:Error = new Error();
		error.name = errorName;
		error.message = errorMsg;
		response.send(error);
	}
}
