package mogodev.app.server.search.lunr;

/**
 * ...
 * @author mogodev
 */
extern class Lunr
{
	@:selfCall
	public function lunr(config:Builder->Void):Index;
	
}

extern class Builder {
	
	
	
	function field(entry:Dynamic):Void;
	function ref(string:String):Void; 
	function add(entry:Dynamic):Void;

}

extern class Index {
	
	var fields:Array<Dynamic>;
	function search(value:Dynamic):Array<Dynamic>; 

}