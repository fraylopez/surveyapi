package mogodev.app.server.search;
import js.Error;
import js.Node;
import mogodev.app.model.SurveyRequest.Range;
import mogodev.app.server.search.elasticlunr.ElasticLunr;
import mogodev.app.server.search.elasticlunr.ElasticLunr.Builder;
import mogodev.app.server.search.elasticlunr.ElasticLunr.Index;
import src.mogodev.util.Helper;
import thx.Arrays;

/**
 * ...
 * @author mogodev
 */
class SearchEngine
{
	static private var fieldMap:Map<String,String> = new Map<String,String>();
	
	var searchIndex:Index;
	var builder:Builder;
	var elasticlunr:ElasticLunr;
	var data:Array<Dynamic>;

	public function new(data:Dynamic, ?callback:SearchEngine->Void) 
	{
		//lunr otimized text based search
		elasticlunr = Node.require("elasticlunr");
		
		setData(data, callback);
			
		fieldMap["target.gender"] = "gender";
		fieldMap["target.income.currency"] = "income.currency";
		fieldMap["target.age"] = "age";
		fieldMap["topic"] = "tags";
		fieldMap["target.income.amount"] = "income.amount";
	}

	function setData(data:Dynamic, ?callback:SearchEngine->Void ) {
		this.data = data;
		trace("Building search indexes...");
		searchIndex = elasticlunr.elasticlunr(
			function (builder:Builder):Void 
			{
				this.builder = builder;
				builder.addField('id');
				builder.addField('income.currency');
				builder.addField('income.amount');
				builder.addField('gender');
				builder.addField('age');
				builder.addField('tags');
				builder.addField('country');
				
				var fields = Reflect.fields(data);
				for (f in fields) 
				{
					builder.addDoc(Helper.flattenObject(Reflect.field(data, f)));
				}
				trace("Data indexed!");
				if (callback != null) callback(this);
			});	
	}
	
	public function searchObject(o:Dynamic):Array<Dynamic> {
		var fl = Helper.flattenObject(o);
		var indexedSearchObject = null;	//contains indexed fields
		var restObject = null;	//contains the rest of the fields
		
		var mappedField = "";
		for (f in Reflect.fields(fl)) 
		{
			mappedField = mapFields(f);
			if (searchIndex._fields.indexOf(mappedField) > -1) {
				if(indexedSearchObject == null) indexedSearchObject = {}
				Reflect.setField(indexedSearchObject, mappedField, Reflect.field(fl, f));
			}
			else 
			{
				var isRange:Bool = Arrays.last(f.split(".")) == "max" || Arrays.last(f.split(".")) == "min"; 
				if (isRange) {
					
					var rangeField:String = f.substring(0, f.lastIndexOf("."));
					mappedField = mapFields(rangeField);
					if (indexedSearchObject != null && Reflect.hasField(indexedSearchObject, mappedField)) continue; //already done
					
					var range:Range = {max: Reflect.field(fl, rangeField + ".max"), min:Reflect.field(fl, rangeField + ".min")};
					
					if (range.max == range.min && range.max != null) {
						//simplify field by collapsing
						if (indexedSearchObject == null) indexedSearchObject = { };
						Reflect.setField(indexedSearchObject, mappedField, cast range.max);
						
						continue;
					}
				}
				
				if(restObject == null) restObject = {}
				Reflect.setField(restObject, f , Reflect.field(fl, f));
			}
		}
		
		var result:Array<Dynamic> = null;
		if (indexedSearchObject != null) {
			result = searchObjectIndexed(indexedSearchObject);
		}
	
		if (restObject != null) {
			//search without lunr
			result = searchObjectDirect(restObject, result);
		}
		return result;
	}
	
	function searchObjectDirect(o:Dynamic, source:Array<Dynamic> = null):Array<Dynamic> 
	{
		source = source != null ? source : this.data;

		var range:Range;
		var rangeField:String = ""; 	
		var mappedField:String = "";
		
		var result:Array<Dynamic> = null;
		
		for (f in Reflect.fields(o)) 
		{
			var isRange:Bool = Arrays.last(f.split(".")) == "max" || Arrays.last(f.split(".")) == "min"; 
			if (isRange) {
				if (result != null) continue; //already done
				
				rangeField = f.substring(0, f.lastIndexOf("."));
				var max = Reflect.field(o, rangeField + ".max");
				var min = Reflect.field(o, rangeField + ".min");
				
				range = {max: max != null ? Std.parseFloat(max) : cast 0x3FFFFFFF, min: min != null ? Std.parseFloat(min) : 0};
				mappedField = mapFields(rangeField);
				
				result = source.filter(
					function (v:Dynamic):Bool 
					{
						var data = Reflect.hasField(v, 'data') ? v.data : v;
						var propArray:Array<String> = mappedField.split(".");
						var v = function ():Float 
						{
							o = data;
							while (propArray.length > 0) {
								o = Reflect.field(o, propArray.shift());
							};
							return o;
						}();
						return v >= range.min && v <= range.max;
					});
			}
			else 
			{
				trace('[WARNING] $f is not a valid search index');
			}
		}
		return result;
	}
	
	function mapFields(field:String):Dynamic 
	{
		if (fieldMap.exists(field)) return fieldMap.get(field);
		return field;
	}
	
	function searchObjectIndexed(o:Dynamic):Array<Dynamic> 
	{
		var result:Array<Dynamic> = null;
		
		for (i in Reflect.fields(o)) 
		{
			if (result == null) {
				result = searchByField(i, Reflect.field(o, i));
			}
			else {
				result = Helper.intersectByField(
					result, 
					searchByField(
						i, 
						Reflect.field(o, i)),
					function (a:Dynamic):Dynamic 
					{
						return Reflect.field(a, "data");
					},
					"id");
			}
		}
		return result;
	}
	
	public function searchByField(f:String, v:String):Array<Dynamic> {
		
		var fields = { };
		Reflect.setField(fields, f, {boost:1});
		return search(v, fields);
	}
	
	public function search(v:String, fields:Dynamic):Array<Dynamic> {
		if (this.searchIndex == null) {
			throw new Error("data needs to be setted before performing a search");
		}
		var lunrResult:Array<Dynamic> = this.searchIndex.search(v, fields);
		var result:Array<Dynamic> = new Array<Dynamic>();
		for (r in lunrResult) 
		{
			result.push({score:r.score, data:this.data[r.ref]});
		}
		return result;
	}
	
}