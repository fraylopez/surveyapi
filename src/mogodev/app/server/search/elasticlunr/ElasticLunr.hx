package mogodev.app.server.search.elasticlunr;

/**
 * ...
 * @author mogodev
 */
extern class ElasticLunr
{
	@:selfCall
	public function elasticlunr(config:Builder->Void):Index;
	
}

extern class Builder {
	
	
	
	function addField(entry:Dynamic):Void;
	function setRef(string:String):Void; 
	function addDoc(entry:Dynamic):Void;

}

extern class Index {
	
	var documentStore :DocumentStore;
	var _fields:Array<Dynamic>;
	function search(value:Dynamic, options:Dynamic = null):Array<Dynamic>; 

}

extern class DocumentStore {
	
	var docs :Dynamic;
}