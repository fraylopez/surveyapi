package mogodev.app.server;

import abe.App;
import express.Express;
import haxe.Json;
import js.Error;
import js.node.Fs;
import mogodev.app.config.Config;
import mogodev.app.server.search.SearchEngine;
import src.mogodev.app.server.routes.RouteHandler;
		
/**
 * ...
 * @author victor mogo
 */

@:expose
class Server 
{
	static function main() 
	{
		loadSampleData();
		
	}
	
	static private function loadSampleData() 
	{
		//async load sample data
		trace("Loading sample data...");
		
		Fs.readFile(Config.DATA_PATH,'utf8',
			function (error:Error, d:String):Void 
			{
				var data = Json.parse(d);
				trace("Data Loaded!");
				
				//create search engine
				createSearchEngine(data);
			});
	}
	
	static private function createSearchEngine(data:Dynamic) 
	{
		new SearchEngine(data, createServer);
	}
	
	static private function createServer(searchEngine:SearchEngine) 
	{
		var app = new abe.App ();

		//serve static html for testing purposes
		app.use('/test', Express.serveStatic(Config.CLIENT_PATH)); 
		app.use('/test-react', Express.serveStatic(Config.CLIENT_REACT_PATH)); 
		app.router.register(new RouteHandler(searchEngine));
		
		app.http(Config.PORT); // running on port 9998
		
		var url = Config.TEST_ENDPOINT;
		var urlReact = Config.TEST_REACT;
		trace('Navigate to $url or $urlReact to test the API');
	}
	
}