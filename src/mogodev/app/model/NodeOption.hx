package mogodev.app.model;

/**
 * ...
 * @author mogodev
 */
class NodeOption
{
	public var description:String;
	public var path:String;
	public var label:String;
	public var schema:Dynamic;

	public function new(label:String, path:String, description:String, ?schema:Dynamic) 
	{
		this.description = description;
		this.path = path;
		this.label = label;
		this.schema = schema;
		
	}
	
}