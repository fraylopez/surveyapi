package mogodev.app.model;

/**
 * ...
 * @author mogodev
 */
typedef SurveyRequest = {
	//requester:Requester,
	//provider:Provider,
	?survey:Survey,
	?subscription:Subscription
}
typedef Survey = {
	?topic:String,
	?target:Target,
	?country:Country
}

typedef Target = {
	?gender:Gender,
	?age:Range,
	?income:Income
}

typedef Income = {
	?amount:Range,
	?currency:Currency
}

enum Gender {
  male;
  female;
}

typedef Range = {
	?min:Float,
	?max:Float,
}

enum Currency {
  EUR;
  DOLLAR;
}

enum Country {
	Spain;
	Vatican;
	France;
	Germany;
	Congo;
	Tobago;
}

typedef Subscription = {
	//subscribe:Bool,
	?frequency:Frequency,
	?channel:Array<Channel>
}

enum Frequency {
	weekly;
	monthly;
}

enum Channel {
	postal;
	mail;
	api;
	ftp;
}



/*typedef Requester = {
	id:String,
	name:String,
}

typedef Provider = {
	id:String,
	name:String,
}
*/

