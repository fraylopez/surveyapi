package mogodev.app.clients.restBridge;
import js.Lib;
import mogodev.app.config.Config;

/**
 * ...
 * @author mogodev
 */


class RestBridge
{
	var api:Dynamic;
	
	public function new() 
	{
		api = Lib.require("restler");
	}
	
	public function get(path:String, value:String = null, callback:Dynamic = null):Void 
	{
		var fullpath = Config.API_ENDPOINT + path + "/" + (value != null ? value : "");
		api.get(fullpath).on('complete', function(result) {
		   if (isError(result)) {
			trace('Error:', result.message);
			if (callback != null) callback(null);
		  } else {
			if (callback != null) callback(result);
		  }
		});
	}
	
	function isError(result:Dynamic) 
	{
		var type = Type.typeof(result);

		return Std.string(result).indexOf("<!DOCTYPE html>") > -1;
	}
}