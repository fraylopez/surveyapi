package mogodev.app.clients.nodeRestClient;
import haxe.extern.EitherType;

/**
 * ...
 * @author victor mogo
 */

@:jsRequire("node-rest-client", "Client")
extern class NodeRestClient
{	
	function new();
	
	function get(string:String, callback:RestResponse):Void; 
	function options(string:String, callback:RestResponse):Void; 
	
}

typedef RestResponse = EitherType<Dynamic, Dynamic>;