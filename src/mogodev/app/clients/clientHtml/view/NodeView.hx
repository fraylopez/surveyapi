package mogodev.app.clients.clientHtml.view;
import js.Browser;
import js.html.ButtonElement;
import js.html.DivElement;
import js.html.Element;
import js.html.HtmlElement;
import js.html.SpanElement;
import js.html.UListElement;
import mogodev.app.clients.clientHtml.model.IView;
import mogodev.app.clients.clientHtml.model.NodeData;

/**
 * ...
 * @author mogodev
 */
class NodeView implements IView
{
	var model:NodeData;
	var view:HtmlElement;
	var content:HtmlElement;
	var label:SpanElement;
	var description:SpanElement;
	var btn:ButtonElement;
	
	
	var children:Map<String, IView>;
	var editor:DivElement;
	var editorControl:Dynamic;
	var hasForm:Bool = false;
	var loading:js.html.LabelElement;
	
	public function new(model:NodeData) 
	{
		this.model = model;
		this.model.onChange.add(render);
	}
	
	public function render():HtmlElement {
		
		if (this.view == null) {
			//creation
			this.view = cast Browser.document.createDivElement();
			view.classList.add("node-view");
			
			this.content = cast Browser.document.createDivElement();
			this.content.setAttribute("id", "content_" + this.model.id);
			this.content.classList.add("node-content");
			
			description = this.view.ownerDocument.createSpanElement();
			description.classList.add("description");
			description.textContent = this.model.description;
			
			btn = this.view.ownerDocument.createButtonElement();
			btn.textContent = this.model.label;
			btn.classList.add("action-button");
			btn.onclick = toggleNode;
			
			this.view.appendChild(btn);
			this.view.appendChild(description);
		}
		
		if (!this.view.contains(this.content)) {
			this.view.appendChild(this.content);
		}
		
		if (this.model.isLoading) {
			loading = Browser.document.createLabelElement();
			loading.setAttribute("id", "loading");
			loading.textContent = "loading...";
			this.content.appendChild (loading);
			return this.view;
		}
		else {
			if (this.loading != null) this.loading.remove();
			
		}
		
		var ul:UListElement;
		if (this.model.isActive) {
			btn.classList.remove("collapsed");
						
			if (this.model.children != null && model.children.length > 0) {
				
				ul = this.view.ownerDocument.createUListElement();
				this.content.appendChild(ul);
				
				for (n in model.children) 
				{
					this.addChild(n, ul);
				}
			}
			else if(this.model.isCallable)
			{
				addFields();
			}
			
			if (this.model.data != null) {
			editor = cast this.content.querySelector('#jsoneditor_' + model.data.id);
			if (editor == null) {
					editor = this.view.ownerDocument.createDivElement();
					editor.setAttribute("id", 'jsoneditor_' + model.data.id);
					editor.classList.add("jsoneditor-container");
					
					this.content.appendChild(editor);
				}
				if (editorControl == null) {
					editorControl  = untyped __js__('new JSONEditor({0}, {})', editor);		
				}
				editorControl.set(this.model.data);
			}
		}
		else 
		{
			remove();
		}
		
		return this.view;
	}
	
	function addChild(childData:NodeData, ul:UListElement) 
	{
		if (this.children == null) this.children = new Map<String, IView>();
		
		if (this.children.exists(childData.id)) return; //already rendered
		
		var child:NodeView =  new NodeView(childData);
		this.children.set(childData.id, child);
		
		var li:Element = this.view.ownerDocument.createLIElement();
		li.appendChild(child.render());
		ul.appendChild(li);
	}
	
	function addFields():Void 
	{
		if (this.children == null) this.children = new Map<String, IView>();
		
		if (this.children.exists(this.model.id)) return; //already rendered
		
		this.hasForm = true;
		var fields:FieldsView = new FieldsView(this.model.schema, this.content);
		fields.onPressRequest.add(this.makeRequest);
		
		this.children.set(this.model.id, fields);
	}
	
	function makeRequest(value:String) 
	{
		this.model.request(value);
	}
	
	public function remove():Void 
	{
		//clean up
		btn.classList.add("collapsed");

		if (editor != null) {
			editor.remove();
			editor = null;
			editorControl = null;
		}
		
		while (this.content.hasChildNodes()) {
			this.content.removeChild(this.content.lastChild);
		}
		
		if (this.children != null) {
			for (i in this.children) 
			{
				i.remove();
				i.dispose();
			}
		}
		this.content.remove();
		this.children = null;
		
		if (this.model.children != null ) {
			for (c in this.model.children) 
			{
				if (c.isActive)  c.toggle();
			}
			
		}
		this.model.invalidate();
		this.hasForm = false;
	}
	
	public function dispose():Void 
	{
		this.model.onChange.removeAll();
	}
	
	function toggleNode() 
	{
		this.model.toggle();
	}
	
	function callMethod(value:String) 
	{
		this.model.request(value);
	}
	
	
	
	
	
}