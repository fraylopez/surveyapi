package mogodev.app.clients.clientHtml.view;
import haxe.Json;
import js.Browser;
import js.html.ButtonElement;
import js.html.DivElement;
import js.html.Element;
import js.html.Event;
import js.html.HtmlElement;
import js.html.LabelElement;
import js.jquery.JQuery;
import mogodev.app.clients.clientHtml.model.IView;
import msignal.Signal.Signal1;

/**
 * ...
 * @author mogodev
 */
class FieldsView implements IView
{
	public var onPressRequest:Signal1<String> = new Signal1();	
	
	var schema:Dynamic;
	var view:DivElement;
	var id:String;
	var title:String;
	var model:Dynamic = {};
	var callButton:ButtonElement;
	var isComposed:Bool;
	var label:LabelElement;

	public function new(schema:Dynamic, parentElement:HtmlElement = null) 
	{
		this.schema = schema;
		this.view = Browser.document.createDivElement();
		this.id = "fields_" + parentElement.getAttribute("id");
		this.view.setAttribute("id", this.id);
		
		if(parentElement != null) parentElement.appendChild(this.view);
		
		this.isComposed = schema.type == null;
		render();
	}
	
	public function render():Element
	{
		var ref:String = Reflect.getProperty(schema, "$ref");
		title = ref != null ? (ref.substring(ref.lastIndexOf("/") + 1, ref.length)) : "";
		var options = {
			"method" : "",
			"action" : "",
			"root" : title,
			"hook" : '#'+this.id,
			"render-options" : {
				"render-types" : {
					"enum" : "select",
					"boolean": "radio"
				}
			}
		};

		var jrender = untyped __js__('jRender({0}, {1})', schema, options);

		callButton = Browser.document.createButtonElement();
		callButton.classList.add("call-button");
		callButton.textContent = "Call me";
		callButton.onclick = clickHandler;
		
		this.view.appendChild(callButton);
		
		new JQuery().ready(this.hookListeners);
		
		return this.view;
	}
	
	
	function hookListeners() 
	{
		new JQuery(this.view).contents().find("select").on("change", formChangeHandler);
		new JQuery(this.view).contents().find('input').on("change", formChangeHandler);
	}
	
	private function formChangeHandler(e:Event):Void 
	{
		//trace(e);
		var el:HtmlElement =  cast e.currentTarget;
		var divWithPropertyName:Element = el;
		var property:String = "";
		
		var propName:String = divWithPropertyName.getAttribute("name");
		while (propName  != this.title) 
		{
			if (propName != null) {
				property = property == "" ? propName : '$propName.$property';
			}
			divWithPropertyName = divWithPropertyName.parentElement;
			propName = divWithPropertyName.getAttribute("name");
		}
		
		var type:String = untyped el.type;
		var value:Dynamic = null;
		switch (type) 
		{
			case "radio":
				value = untyped el.value == "True";
	
			case "checkbox":
				var checked = el.parentElement.querySelectorAll('input:checked');
				value = new Array();
				for (i in checked) 
				{
					value.push(untyped i.value);
				}
				
			case "select-one":
				value = untyped el.selectedOptions[0].value;
				
			case "text":
				var name = untyped el.name;
				value = untyped el.value;
		}
		
		if (property == "") this.model = value;
		else				updateModel(this.model, property.split("."), value);
	}
	
	function updateModel(o:Dynamic, propArray:Array<String>, value:String):Dynamic 
	{
		if (propArray.length > 1) {
			var currentProp = propArray.shift();
			var currentObj = Reflect.field(o, currentProp);
			Reflect.setField(o, currentProp, updateModel(currentObj != null ? currentObj : { },  propArray , value ));	
			if (currentObj != null && Reflect.fields(currentObj).length == 0) {
				Reflect.deleteField(o, currentProp );	
			}
		}
		else {
			if(value == null || value == "")
				Reflect.deleteField(o, propArray[0]);	
			else
				Reflect.setField(o, propArray[0], value );	
		}
		return o;
	}
	
	
	function deleteField(propArray:Array<String>) 
	{
		var parents:Map<String,Dynamic> = new Map<String, Dynamic>();

		var nested = this.model;
		var p = "";
		var props = propArray.copy();
		while (props.length > 0) 
		{
			p = props.shift();
			parents[p] = nested;
			nested = Reflect.field(nested, p);
		}
		
		props = propArray.copy();
		var parent = parents[props.pop()];
		while (Reflect.fields(parent).length == 1) 
		{
			Reflect.deleteField(parent, p);
			parent = parents[props.pop()];
		}
	}
	
	
	function clickHandler(e:Event) 
	{
		e.preventDefault();
		if (label != null) label.remove();
		
		if (this.model == null || Reflect.fields(this.model).length == 0) {
			label = Browser.document.createLabelElement();
			label.classList.add("no-model");
			label.textContent = "Please, enter at least one field";
			this.view.appendChild(label);
		}
		else {
			this.onPressRequest.dispatch(this.isComposed ? Json.stringify(this.model): this.model);
		}
	}
	
	public function remove():Void 
	{
		while (this.view.hasChildNodes()) {
			this.view.removeChild(this.view.lastChild);
		}
		this.view.remove();
	}
	
	public function dispose():Void 
	{
		this.onPressRequest.removeAll();
	}
	
	
}