package mogodev.app.clients.clientHtml.view;

import js.html.Element;
import mogodev.app.clients.clientHtml.model.NodeData;

/**
 * ...
 * @author mogodev
 */
class ApiExplorer
{
	var appContainer:Element;

	public function new(appContainer:Element) 
	{
		this.appContainer = appContainer;
		var rootNodeData:NodeData = 
			new NodeData(
				"0", 
				"API Root", 
				"", 
				"This is a demo app to explore a REST API." +
				"The goal is to retrieve Market Surveys." +
				"This application allows to navigate the API nodes and create request through auto-generated forms.");
				
		var rootNode:NodeView = new NodeView(rootNodeData);
		
		appContainer.appendChild(rootNode.render());
	}

	
}