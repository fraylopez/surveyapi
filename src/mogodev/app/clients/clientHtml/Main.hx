package mogodev.app.clients.clientHtml;

import js.Browser;
import mogodev.app.clients.clientHtml.view.ApiExplorer;
import mogodev.app.clients.restBridge.RestBridge;

/**
 * ...
 * @author victor mogo
 */

@:expose
class Main
{
	static public inline var API_ENTRY_POINT:String = "http://localhost:9998/";
	static public var api:RestBridge;
	
	//Entry point
	static function main() {
			
        //expose rest client to test from browser side
		//untyped Browser.window.ClientApp = RestBridge;
		
		api = new RestBridge();
		Browser.window.onload = init;
		
    }
	
	static private function init() 
	{
		var container =  Browser.document.getElementById('app');
		new ApiExplorer(container);
	}
	
}