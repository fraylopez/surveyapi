package mogodev.app.clients.clientHtml.model;
import js.html.Element;

/**
 * ...
 * @author mogodev
 */
interface IView
{
	function render():Element; 
	function remove():Void; 
	function dispose():Void; 
}