package mogodev.app.clients.clientReact;

import js.Browser;
import mogodev.app.clients.restBridge.RestBridge;
import react.ReactDOM;
import react.ReactMacro.jsx;
import mogodev.app.clients.clientReact.view.SurveyApp;

/**
 * ...
 * @author mogodev
 */
class Main
{
	static public var api:RestBridge;
	
	public static function main()
	{
		//untyped Browser.window.api = rest;
		Browser.window.onload = createApp;
	}
	
	static private function createApp() 
	{
		api = new RestBridge();
		ReactDOM.render(jsx('<$SurveyApp/>'), Browser.document.getElementById('app'));
	}
	
}