package mogodev.app.clients.clientReact.view;
import haxe.Json;
import mogodev.app.clients.clientReact.model.NodeData;
import react.ReactComponent.ReactComponentOfPropsAndState;
import react.ReactMacro.jsx;
/**
 * ...
 * @author mogodev
 */

typedef NodeProps = {
	model:NodeData
};

typedef NodeState = {
	isOpen:Bool,
	?data:Dynamic,
	?children:Array<NodeData>
};

class ApiNode extends ReactComponentOfPropsAndState<NodeProps, NodeState>
{
	static var theme = {
		scheme: 'monokai',
		author: 'wimer hazenberg (http://www.monokai.nl)',
		base00: '#272822',
		base01: '#383830',
		base02: '#49483e',
		base03: '#75715e',
		base04: '#a59f85',
		base05: '#f8f8f2',
		base06: '#f5f4f1',
		base07: '#f9f8f5',
		base08: '#f92672',
		base09: '#fd971f',
		base0A: '#f4bf75',
		base0B: '#a6e22e',
		base0C: '#a1efe4',
		base0D: '#66d9ef',
		base0E: '#ae81ff',
		base0F: '#cc6633'
	};

	public function new(props:NodeProps)
	{
		super(props);
		
		this.state = { isOpen:props.model.isActive, data:props.model.data, children:props.model.children } ;
		props.model.onChange.add(function ():Void 
		{
			setState( { isOpen:props.model.isActive, data:props.model.data, children:props.model.children } );
		});
	}
	
	override public function componentWillUnmount():Void 
	{
		props.model.onChange.removeAll(); //safety first
	}
	
	override public function render() 
	{
		return jsx('
			<div className="node-view">
				{renderButton()}
				<span className="description">{props.model.description}</span>
				{renderContents() }
				{renderData() }
			</div>	
		');
	}
	
	function renderButton() 
	{
		var btnClasses = ['action-button'];
		if (!this.state.isOpen) btnClasses.push('collapsed');
		
		return jsx('<button className={btnClasses.join(" ")} onClick={this.clickHandler}>{props.model.label}</button>');
	}

	function renderContents() 
	{
		if (this.state.isOpen) {
			if (state.children != null && state.children.length > 0) {
				//var childNodes:Array<NodeData> = []
				return [ 
						for (c in state.children) 
						{
							jsx('<$ApiNode key={c.id} model={c}/>');
						}
					];
			}
			else if(this.state.isOpen && props.model.schema != null) {
				return [addForm(props.model.schema)];
			}
		}
		
		return null;
	}
	
	
	function addForm(schema:Dynamic)
	{
		return jsx('<$Form key={props.model.id} schema={schema} onSubmit={submitHandler}/>');
	}

	function submitHandler(data:Dynamic)
	{
		//trace(data.formData);
		this.props.model.request(Json.stringify(data.formData));
	}
	
	
	function renderData() 
	{
		if(this.state.isOpen && state.data != null) {
	
			return jsx('
						<div className="request-result">
							<span>RESULT:</span>
							<$JsonTree data = { state.data } theme = { theme } invertTheme = { true } />
						</div>
					');
		}
		
		return null;
	}
	
	function clickHandler() 
	{
		//this.setState( { isOpen: !this.state.isOpen } );
		
		this.props.model.toggle();
	}
	
}
