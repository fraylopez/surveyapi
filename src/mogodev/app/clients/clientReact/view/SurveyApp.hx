package mogodev.app.clients.clientReact.view;

/**
 * ...
 * @author mogodev
 */
import mogodev.app.clients.clientReact.model.NodeData;
import react.ReactComponent;
import react.ReactMacro.jsx;



class SurveyApp extends ReactComponent
{
	var rootNodeData:NodeData;
	
	public function new(props:Dynamic)
	{
		super(props);
		rootNodeData = 
			new NodeData(
				"0", 
				"API Root", 
				"", 
				"This is a demo app to explore a REST API." +
				"The goal is to retrieve Market Surveys." +
				"This application allows to navigate the API nodes and create request through auto-generated forms.");
	}

	override public function render() 
	{
		//var unchecked = state.items.filter(function(item) return !item.checked).length;
		
		
		return jsx('
			< div className = "app" style = {{ margin:"10px" }} >
				<h1>Explore this API!</h1>
				<$ApiNode model={rootNodeData}/>
			</div>
		');
	}
}
	
