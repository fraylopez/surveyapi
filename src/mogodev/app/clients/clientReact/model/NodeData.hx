package mogodev.app.clients.clientReact.model;
import msignal.Signal.Signal0;

/**
 * ...
 * @author mogodev
 */
class NodeData 
{
	public var onChange:Signal0 = new Signal0();
	
	public var id:String;
	public var label:String;
	public var path:String;
	public var description:String;
	public var isActive:Bool = false;
	public var isLoading:Bool = false;
	public var isCallable:Bool = false;
	public var schema:Dynamic;
	public var data:Dynamic;
	
	var isValid:Bool;
	
	
	@:isVar public var children(default, set):Array<NodeData>;
	
	
	public function new(id:String, label:String, path:String, description:String, ?schema:Dynamic) 
	{
		this.schema = schema;
		this.description = description;
		this.path = path;
		this.label = label;
		this.id = id;
		
	}

	
	public function toggle() {
		this.isActive = !this.isActive;
		onChange.dispatch();
		
		if (!this.isActive || this.children != null) return; //closed or cached
		
		//request options
		//self define api!
		
		this.isLoading = true;
		onChange.dispatch();
		
		Main.api.get(this.path, 
			function (options:Array<Dynamic> = null):Void 
			{
				this.isLoading = false;
				if (options != null && options.length > 0) {
					if (children == null) children = new Array<NodeData>();
					for (o in options) 
					{
						var id = this.id + "_" + this.children.length;
						var path = this.path +"/" + o.path;
						this.children.push(new NodeData(id, o.label, path, o.description, o.schema));
					}
				}
				else 
				{
					this.isCallable = true;
				}
				this.onChange.dispatch();
			});
		
	}
	
	public function request(value:String) 
	{
		//untyped __js__('waitingDialog.show()');
		Main.api.get(
			this.path, 
			value,
			function (data:Dynamic):Void 
			{
				//untyped __js__('waitingDialog.hide()');
				this.data = data;
				this.onChange.dispatch();
			});
	}
	
	public function invalidate() 
	{
		this.data = null;
	}

	function set_children(value:Array<NodeData>):Array<NodeData> 
	{
		this.children = value;
		this.onChange.dispatch();
		return this.children;
	}
	

	
}