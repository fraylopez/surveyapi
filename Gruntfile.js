module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
	clean: {
		build: {
			src: 'build'
		}
	},
	copy: {
		main: {
			src: [
				'**/*',  
				'!client-react/node_modules/**',
				'!client-react/js/**',
			
				'!client-react/**/*.scss',
				
				'!client/node_modules/**',
				'!client/js/**',
			
				'!client/**/*.scss',
				'!client/package.json',
				
				'!**/*.map',
				
				'!server/node_modules/**',
				'!server/data/sampleData.jsonGenerator'
				
			],
            expand: true,
            cwd: 'app',
            dest: 'build'
		}
	
	},
	browserify: {
		html: {
			src:['app/client/js/hx/client.js'],
			dest:'build/client/js/client.js'
		},
		react: {
			src:['app/client-react/js/hx/client.js'],
			dest:'build/client-react/js/client.js'
		}
		
	},
	uglify: {
		html: {
			src:['build/client/js/client.js'],
			dest:'build/client/js/client.js'
		},
		react: {
			src:['build/client-react/js/client.js'],
			dest:'build/client-react/js/client.js'
		}
	  }
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Default task(s).
  grunt.registerTask('build', ['clean','copy','browserify:html', 'browserify:react','uglify:html', 'uglify:react']);
  grunt.registerTask('ug', ['uglify:html', 'uglify:react']);

};