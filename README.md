# HaxeJS-Node REST API

This is a demo implementation of a REST API and a test client made in Haxe. The API is self defined and can be navigated with the provided test client. The problem to solve in this demo scenario is getting data about available market surveys.

The API exploration relies on NodeOption arrays as responses to some GET requests to an API route. The NodeOption provides some descriptive information and a child route to dig further. It can also contain a json schema object meaning that the call requires a parameter.

The client uses this NodeOption objects to generate the views. It renders lists for the child routes and forms when a json schema is received. The form submision creates the final request to the API.

For demo purposes the requests can be SINGLE (search  by 1 parameter) or COMPOSED (multi paramter search).

[Haxe](https://haxe.org/) is an open source toolkit based on a modern, high level, strictly typed programming language, a cross-compiler, a complete cross-platform standard library and ways to access each platform's native capabilities.


NOTE: The app uses localhost port 9998 and it should be free to launch the test. 
    
Install dependencies on server and react client and test:

```javascript
npm run setup-start
```

If dependencies are instaled the app can be launched through:

```javascript
npm start
```

The test executables are in build/ dir

2 test clients are provided: build/client/ is plain html+js while build/client-react uses the react framework and some components.


This is the full technology stack

* Haxe
	* Server
		* [Nodejs](https://nodejs.org): No presentation required...
		* [Express](http://expressjs.com): Node framework
		* [Abe](https://github.com/abedev/abe): REST api builder on top of express
		* [Elasticlunr](http://elasticlunr.com/): Lightweight search engine

	
	* Client 
		* [Restler](https://github.com/danwrong/restler): Rest consuming client
		* [Bootstrap](http://getbootstrap.com/): Well... bootstrap	
			
		* Plain HaxeJS Client
			
			* [Jsoneditor](https://github.com/josdejong/jsoneditor): Nicely display and explore Json 
			* [jRender](https://github.com/vu2srk/jRender): Render Json schemas
			
		
		* React Client
			* [react](https://facebook.github.io/react/): Facebook ui builder framework
			* [Haxe-React](https://github.com/massiveinteractive/haxe-react): React externs to be used with Haxe
			* [react-jsonschema-form](https://github.com/mozilla-services/react-jsonschema-form): Form from json schema
			* [react-json-tree](https://github.com/alexkuz/react-json-tree): json object render
