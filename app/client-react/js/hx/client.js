// Generated by Haxe 3.4.2 (git build master @ 890f8c7)
(function ($global) { "use strict";
var $estr = function() { return js.Boot.__string_rec(this,''); };
function $extend(from, fields) {
	function Inherit() {} Inherit.prototype = from; var proto = new Inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
var HxOverrides = function() { };
HxOverrides.__name__ = true;
HxOverrides.iter = function(a) {
	return { cur : 0, arr : a, hasNext : function() {
		return this.cur < this.arr.length;
	}, next : function() {
		return this.arr[this.cur++];
	}};
};
Math.__name__ = true;
var Reflect = function() { };
Reflect.__name__ = true;
Reflect.isFunction = function(f) {
	if(typeof(f) == "function") {
		return !(f.__name__ || f.__ename__);
	} else {
		return false;
	}
};
Reflect.compareMethods = function(f1,f2) {
	if(f1 == f2) {
		return true;
	}
	if(!Reflect.isFunction(f1) || !Reflect.isFunction(f2)) {
		return false;
	}
	if(f1.scope == f2.scope && f1.method == f2.method) {
		return f1.method != null;
	} else {
		return false;
	}
};
var Std = function() { };
Std.__name__ = true;
Std.string = function(s) {
	return js.Boot.__string_rec(s,"");
};
var ValueType = { __ename__ : true, __constructs__ : ["TNull","TInt","TFloat","TBool","TObject","TFunction","TClass","TEnum","TUnknown"] };
ValueType.TNull = ["TNull",0];
ValueType.TNull.toString = $estr;
ValueType.TNull.__enum__ = ValueType;
ValueType.TInt = ["TInt",1];
ValueType.TInt.toString = $estr;
ValueType.TInt.__enum__ = ValueType;
ValueType.TFloat = ["TFloat",2];
ValueType.TFloat.toString = $estr;
ValueType.TFloat.__enum__ = ValueType;
ValueType.TBool = ["TBool",3];
ValueType.TBool.toString = $estr;
ValueType.TBool.__enum__ = ValueType;
ValueType.TObject = ["TObject",4];
ValueType.TObject.toString = $estr;
ValueType.TObject.__enum__ = ValueType;
ValueType.TFunction = ["TFunction",5];
ValueType.TFunction.toString = $estr;
ValueType.TFunction.__enum__ = ValueType;
ValueType.TClass = function(c) { var $x = ["TClass",6,c]; $x.__enum__ = ValueType; $x.toString = $estr; return $x; };
ValueType.TEnum = function(e) { var $x = ["TEnum",7,e]; $x.__enum__ = ValueType; $x.toString = $estr; return $x; };
ValueType.TUnknown = ["TUnknown",8];
ValueType.TUnknown.toString = $estr;
ValueType.TUnknown.__enum__ = ValueType;
var Type = function() { };
Type.__name__ = true;
Type["typeof"] = function(v) {
	var _g = typeof(v);
	switch(_g) {
	case "boolean":
		return ValueType.TBool;
	case "function":
		if(v.__name__ || v.__ename__) {
			return ValueType.TObject;
		}
		return ValueType.TFunction;
	case "number":
		if(Math.ceil(v) == v % 2147483648.0) {
			return ValueType.TInt;
		}
		return ValueType.TFloat;
	case "object":
		if(v == null) {
			return ValueType.TNull;
		}
		var e = v.__enum__;
		if(e != null) {
			return ValueType.TEnum(e);
		}
		var c = js.Boot.getClass(v);
		if(c != null) {
			return ValueType.TClass(c);
		}
		return ValueType.TObject;
	case "string":
		return ValueType.TClass(String);
	case "undefined":
		return ValueType.TNull;
	default:
		return ValueType.TUnknown;
	}
};
var haxe = {};
haxe.Log = function() { };
haxe.Log.__name__ = true;
haxe.Log.trace = function(v,infos) {
	js.Boot.__trace(v,infos);
};
haxe.io = {};
haxe.io.Bytes = function() { };
haxe.io.Bytes.__name__ = true;
haxe.io.Bytes.prototype = {
	__class__: haxe.io.Bytes
};
var js = {};
js._Boot = {};
js._Boot.HaxeError = function(val) {
	Error.call(this);
	this.val = val;
	this.message = String(val);
	if(Error.captureStackTrace) {
		Error.captureStackTrace(this,js._Boot.HaxeError);
	}
};
js._Boot.HaxeError.__name__ = true;
js._Boot.HaxeError.wrap = function(val) {
	if((val instanceof Error)) {
		return val;
	} else {
		return new js._Boot.HaxeError(val);
	}
};
js._Boot.HaxeError.__super__ = Error;
js._Boot.HaxeError.prototype = $extend(Error.prototype,{
	__class__: js._Boot.HaxeError
});
js.Boot = function() { };
js.Boot.__name__ = true;
js.Boot.__unhtml = function(s) {
	return s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
};
js.Boot.__trace = function(v,i) {
	var msg = i != null ? i.fileName + ":" + i.lineNumber + ": " : "";
	msg += js.Boot.__string_rec(v,"");
	if(i != null && i.customParams != null) {
		var _g = 0;
		var _g1 = i.customParams;
		while(_g < _g1.length) {
			var v1 = _g1[_g];
			++_g;
			msg += "," + js.Boot.__string_rec(v1,"");
		}
	}
	var d;
	var tmp;
	if(typeof(document) != "undefined") {
		d = document.getElementById("haxe:trace");
		tmp = d != null;
	} else {
		tmp = false;
	}
	if(tmp) {
		d.innerHTML += js.Boot.__unhtml(msg) + "<br/>";
	} else if(typeof console != "undefined" && console.log != null) {
		console.log(msg);
	}
};
js.Boot.getClass = function(o) {
	if((o instanceof Array) && o.__enum__ == null) {
		return Array;
	} else {
		var cl = o.__class__;
		if(cl != null) {
			return cl;
		}
		var name = js.Boot.__nativeClassName(o);
		if(name != null) {
			return js.Boot.__resolveNativeClass(name);
		}
		return null;
	}
};
js.Boot.__string_rec = function(o,s) {
	if(o == null) {
		return "null";
	}
	if(s.length >= 5) {
		return "<...>";
	}
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) {
		t = "object";
	}
	switch(t) {
	case "function":
		return "<function>";
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) {
					return o[0];
				}
				var str = o[0] + "(";
				s += "\t";
				var _g1 = 2;
				var _g = o.length;
				while(_g1 < _g) {
					var i = _g1++;
					if(i != 2) {
						str += "," + js.Boot.__string_rec(o[i],s);
					} else {
						str += js.Boot.__string_rec(o[i],s);
					}
				}
				return str + ")";
			}
			var l = o.length;
			var i1;
			var str1 = "[";
			s += "\t";
			var _g11 = 0;
			var _g2 = l;
			while(_g11 < _g2) {
				var i2 = _g11++;
				str1 += (i2 > 0 ? "," : "") + js.Boot.__string_rec(o[i2],s);
			}
			str1 += "]";
			return str1;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			return "???";
		}
		if(tostr != null && tostr != Object.toString && typeof(tostr) == "function") {
			var s2 = o.toString();
			if(s2 != "[object Object]") {
				return s2;
			}
		}
		var k = null;
		var str2 = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) {
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str2.length != 2) {
			str2 += ", \n";
		}
		str2 += s + k + " : " + js.Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str2 += "\n" + s + "}";
		return str2;
	case "string":
		return o;
	default:
		return String(o);
	}
};
js.Boot.__nativeClassName = function(o) {
	var name = js.Boot.__toStr.call(o).slice(8,-1);
	if(name == "Object" || name == "Function" || name == "Math" || name == "JSON") {
		return null;
	}
	return name;
};
js.Boot.__resolveNativeClass = function(name) {
	return $global[name];
};
js.node = {};
js.node.buffer = {};
js.node.buffer.Buffer = require("buffer").Buffer;
var mogodev = {};
mogodev.app = {};
mogodev.app.clients = {};
mogodev.app.clients.clientReact = {};
mogodev.app.clients.clientReact.Main = function() { };
mogodev.app.clients.clientReact.Main.__name__ = true;
mogodev.app.clients.clientReact.Main.main = function() {
	window.onload = mogodev.app.clients.clientReact.Main.createApp;
};
mogodev.app.clients.clientReact.Main.createApp = function() {
	mogodev.app.clients.clientReact.Main.api = new mogodev.app.clients.restBridge.RestBridge();
	ReactDOM.render(React.createElement(mogodev.app.clients.clientReact.view.SurveyApp,{ }),window.document.getElementById("app"));
};
mogodev.app.clients.clientReact.model = {};
mogodev.app.clients.clientReact.model.NodeData = function(id,label,path,description,schema) {
	this.isCallable = false;
	this.isLoading = false;
	this.isActive = false;
	this.onChange = new msignal.Signal0();
	this.schema = schema;
	this.description = description;
	this.path = path;
	this.label = label;
	this.id = id;
};
mogodev.app.clients.clientReact.model.NodeData.__name__ = true;
mogodev.app.clients.clientReact.model.NodeData.prototype = {
	toggle: function() {
		var _gthis = this;
		this.isActive = !this.isActive;
		this.onChange.dispatch();
		if(!this.isActive || this.children != null) {
			return;
		}
		this.isLoading = true;
		this.onChange.dispatch();
		mogodev.app.clients.clientReact.Main.api.get(this.path,null,function(options) {
			_gthis.isLoading = false;
			if(options != null && options.length > 0) {
				if(_gthis.children == null) {
					_gthis.set_children([]);
				}
				var _g = 0;
				while(_g < options.length) {
					var o = options[_g];
					++_g;
					var id = _gthis.id + "_" + _gthis.children.length;
					var path = _gthis.path + "/" + Std.string(o.path);
					_gthis.children.push(new mogodev.app.clients.clientReact.model.NodeData(id,o.label,path,o.description,o.schema));
				}
			} else {
				_gthis.isCallable = true;
			}
			_gthis.onChange.dispatch();
		});
	}
	,request: function(value) {
		var _gthis = this;
		mogodev.app.clients.clientReact.Main.api.get(this.path,value,function(data) {
			_gthis.data = data;
			_gthis.onChange.dispatch();
		});
	}
	,set_children: function(value) {
		this.children = value;
		this.onChange.dispatch();
		return this.children;
	}
	,__class__: mogodev.app.clients.clientReact.model.NodeData
};
var React = {};
React.Component = require("react").Component;
mogodev.app.clients.clientReact.view = {};
mogodev.app.clients.clientReact.view.ApiNode = function(props) {
	var _gthis = this;
	React.Component.call(this,props);
	this.state = { isOpen : props.model.isActive, data : props.model.data, children : props.model.children};
	props.model.onChange.add(function() {
		_gthis.setState({ isOpen : props.model.isActive, data : props.model.data, children : props.model.children});
	});
};
mogodev.app.clients.clientReact.view.ApiNode.__name__ = true;
mogodev.app.clients.clientReact.view.ApiNode.__super__ = React.Component;
mogodev.app.clients.clientReact.view.ApiNode.prototype = $extend(React.Component.prototype,{
	componentWillUnmount: function() {
		this.props.model.onChange.removeAll();
	}
	,render: function() {
		var tmp = this.renderButton();
		var tmp1 = React.createElement("span",{ className : "description"},this.props.model.description);
		var tmp2 = this.renderContents();
		var tmp3 = this.renderData();
		return React.createElement("div",{ className : "node-view"},tmp,tmp1,tmp2,tmp3);
	}
	,renderButton: function() {
		var btnClasses = ["action-button"];
		if(!this.state.isOpen) {
			btnClasses.push("collapsed");
		}
		return React.createElement("button",{ onClick : $bind(this,this.clickHandler), className : btnClasses.join(" ")},this.props.model.label);
	}
	,renderContents: function() {
		if(this.state.isOpen) {
			if(this.state.children != null && this.state.children.length > 0) {
				var _g = [];
				var _g1 = 0;
				var _g2 = this.state.children;
				while(_g1 < _g2.length) {
					var c = _g2[_g1];
					++_g1;
					_g.push(React.createElement(mogodev.app.clients.clientReact.view.ApiNode,{ key : c.id, model : c}));
				}
				return _g;
			} else if(this.state.isOpen && this.props.model.schema != null) {
				return [this.addForm(this.props.model.schema)];
			}
		}
		return null;
	}
	,addForm: function(schema) {
		return React.createElement(mogodev.app.clients.clientReact.view.Form,{ key : this.props.model.id, onSubmit : $bind(this,this.submitHandler), schema : schema});
	}
	,submitHandler: function(data) {
		this.props.model.request(JSON.stringify(data.formData));
	}
	,renderData: function() {
		if(this.state.isOpen && this.state.data != null) {
			var tmp = React.createElement("span",{ },"RESULT:");
			var tmp1 = React.createElement(mogodev.app.clients.clientReact.view.JsonTree,{ theme : mogodev.app.clients.clientReact.view.ApiNode.theme, invertTheme : true, data : this.state.data});
			return React.createElement("div",{ className : "request-result"},tmp,tmp1);
		}
		return null;
	}
	,clickHandler: function() {
		this.props.model.toggle();
	}
	,__class__: mogodev.app.clients.clientReact.view.ApiNode
});
mogodev.app.clients.clientReact.view.Form = require("react-jsonschema-form").default;
mogodev.app.clients.clientReact.view.JsonTree = require("react-json-tree").default;
mogodev.app.clients.clientReact.view.SurveyApp = function(props) {
	React.Component.call(this,props);
	this.rootNodeData = new mogodev.app.clients.clientReact.model.NodeData("0","API Root","","This is a demo app to explore a REST API." + "The goal is to retrieve Market Surveys." + "This application allows to navigate the API nodes and create request through auto-generated forms.");
};
mogodev.app.clients.clientReact.view.SurveyApp.__name__ = true;
mogodev.app.clients.clientReact.view.SurveyApp.__super__ = React.Component;
mogodev.app.clients.clientReact.view.SurveyApp.prototype = $extend(React.Component.prototype,{
	render: function() {
		var tmp = React.createElement("h1",{ },"Explore this API!");
		var tmp1 = React.createElement(mogodev.app.clients.clientReact.view.ApiNode,{ model : this.rootNodeData});
		return React.createElement("div",{ style : { margin : "10px"}, className : "app"},tmp,tmp1);
	}
	,__class__: mogodev.app.clients.clientReact.view.SurveyApp
});
mogodev.app.clients.restBridge = {};
mogodev.app.clients.restBridge.RestBridge = function() {
	this.api = require("restler");
};
mogodev.app.clients.restBridge.RestBridge.__name__ = true;
mogodev.app.clients.restBridge.RestBridge.prototype = {
	get: function(path,value,callback) {
		var _gthis = this;
		var fullpath = "http://localhost" + ":" + 9998 + "/api" + path + "/" + (value != null ? value : "");
		this.api.get(fullpath).on("complete",function(result) {
			if(_gthis.isError(result)) {
				haxe.Log.trace("Error:",{ fileName : "RestBridge.hx", lineNumber : 25, className : "mogodev.app.clients.restBridge.RestBridge", methodName : "get", customParams : [result.message]});
				if(callback != null) {
					callback(null);
				}
			} else if(callback != null) {
				callback(result);
			}
		});
	}
	,isError: function(result) {
		var type = Type["typeof"](result);
		return Std.string(result).indexOf("<!DOCTYPE html>") > -1;
	}
	,__class__: mogodev.app.clients.restBridge.RestBridge
};
var msignal = {};
msignal.Signal = function(valueClasses) {
	if(valueClasses == null) {
		valueClasses = [];
	}
	this.valueClasses = valueClasses;
	this.slots = msignal.SlotList.NIL;
	this.priorityBased = false;
};
msignal.Signal.__name__ = true;
msignal.Signal.prototype = {
	add: function(listener) {
		return this.registerListener(listener);
	}
	,addOnce: function(listener) {
		return this.registerListener(listener,true);
	}
	,addWithPriority: function(listener,priority) {
		if(priority == null) {
			priority = 0;
		}
		return this.registerListener(listener,false,priority);
	}
	,addOnceWithPriority: function(listener,priority) {
		if(priority == null) {
			priority = 0;
		}
		return this.registerListener(listener,true,priority);
	}
	,remove: function(listener) {
		var slot = this.slots.find(listener);
		if(slot == null) {
			return null;
		}
		this.slots = this.slots.filterNot(listener);
		return slot;
	}
	,removeAll: function() {
		this.slots = msignal.SlotList.NIL;
	}
	,registerListener: function(listener,once,priority) {
		if(priority == null) {
			priority = 0;
		}
		if(once == null) {
			once = false;
		}
		if(this.registrationPossible(listener,once)) {
			var newSlot = this.createSlot(listener,once,priority);
			if(!this.priorityBased && priority != 0) {
				this.priorityBased = true;
			}
			if(!this.priorityBased && priority == 0) {
				this.slots = this.slots.prepend(newSlot);
			} else {
				this.slots = this.slots.insertWithPriority(newSlot);
			}
			return newSlot;
		}
		return this.slots.find(listener);
	}
	,registrationPossible: function(listener,once) {
		if(!this.slots.nonEmpty) {
			return true;
		}
		var existingSlot = this.slots.find(listener);
		if(existingSlot == null) {
			return true;
		}
		if(existingSlot.once != once) {
			throw new js._Boot.HaxeError("You cannot addOnce() then add() the same listener without removing the relationship first.");
		}
		return false;
	}
	,createSlot: function(listener,once,priority) {
		if(priority == null) {
			priority = 0;
		}
		if(once == null) {
			once = false;
		}
		return null;
	}
	,get_numListeners: function() {
		return this.slots.get_length();
	}
	,__class__: msignal.Signal
};
msignal.Signal0 = function() {
	msignal.Signal.call(this);
};
msignal.Signal0.__name__ = true;
msignal.Signal0.__super__ = msignal.Signal;
msignal.Signal0.prototype = $extend(msignal.Signal.prototype,{
	dispatch: function() {
		var slotsToProcess = this.slots;
		while(slotsToProcess.nonEmpty) {
			slotsToProcess.head.execute();
			slotsToProcess = slotsToProcess.tail;
		}
	}
	,createSlot: function(listener,once,priority) {
		if(priority == null) {
			priority = 0;
		}
		if(once == null) {
			once = false;
		}
		return new msignal.Slot0(this,listener,once,priority);
	}
	,__class__: msignal.Signal0
});
msignal.Signal1 = function(type) {
	msignal.Signal.call(this,[type]);
};
msignal.Signal1.__name__ = true;
msignal.Signal1.__super__ = msignal.Signal;
msignal.Signal1.prototype = $extend(msignal.Signal.prototype,{
	dispatch: function(value) {
		var slotsToProcess = this.slots;
		while(slotsToProcess.nonEmpty) {
			slotsToProcess.head.execute(value);
			slotsToProcess = slotsToProcess.tail;
		}
	}
	,createSlot: function(listener,once,priority) {
		if(priority == null) {
			priority = 0;
		}
		if(once == null) {
			once = false;
		}
		return new msignal.Slot1(this,listener,once,priority);
	}
	,__class__: msignal.Signal1
});
msignal.Signal2 = function(type1,type2) {
	msignal.Signal.call(this,[type1,type2]);
};
msignal.Signal2.__name__ = true;
msignal.Signal2.__super__ = msignal.Signal;
msignal.Signal2.prototype = $extend(msignal.Signal.prototype,{
	dispatch: function(value1,value2) {
		var slotsToProcess = this.slots;
		while(slotsToProcess.nonEmpty) {
			slotsToProcess.head.execute(value1,value2);
			slotsToProcess = slotsToProcess.tail;
		}
	}
	,createSlot: function(listener,once,priority) {
		if(priority == null) {
			priority = 0;
		}
		if(once == null) {
			once = false;
		}
		return new msignal.Slot2(this,listener,once,priority);
	}
	,__class__: msignal.Signal2
});
msignal.Slot = function(signal,listener,once,priority) {
	if(priority == null) {
		priority = 0;
	}
	if(once == null) {
		once = false;
	}
	this.signal = signal;
	this.set_listener(listener);
	this.once = once;
	this.priority = priority;
	this.enabled = true;
};
msignal.Slot.__name__ = true;
msignal.Slot.prototype = {
	remove: function() {
		this.signal.remove(this.listener);
	}
	,set_listener: function(value) {
		if(value == null) {
			throw new js._Boot.HaxeError("listener cannot be null");
		}
		return this.listener = value;
	}
	,__class__: msignal.Slot
};
msignal.Slot0 = function(signal,listener,once,priority) {
	if(priority == null) {
		priority = 0;
	}
	if(once == null) {
		once = false;
	}
	msignal.Slot.call(this,signal,listener,once,priority);
};
msignal.Slot0.__name__ = true;
msignal.Slot0.__super__ = msignal.Slot;
msignal.Slot0.prototype = $extend(msignal.Slot.prototype,{
	execute: function() {
		if(!this.enabled) {
			return;
		}
		if(this.once) {
			this.remove();
		}
		this.listener();
	}
	,__class__: msignal.Slot0
});
msignal.Slot1 = function(signal,listener,once,priority) {
	if(priority == null) {
		priority = 0;
	}
	if(once == null) {
		once = false;
	}
	msignal.Slot.call(this,signal,listener,once,priority);
};
msignal.Slot1.__name__ = true;
msignal.Slot1.__super__ = msignal.Slot;
msignal.Slot1.prototype = $extend(msignal.Slot.prototype,{
	execute: function(value1) {
		if(!this.enabled) {
			return;
		}
		if(this.once) {
			this.remove();
		}
		if(this.param != null) {
			value1 = this.param;
		}
		this.listener(value1);
	}
	,__class__: msignal.Slot1
});
msignal.Slot2 = function(signal,listener,once,priority) {
	if(priority == null) {
		priority = 0;
	}
	if(once == null) {
		once = false;
	}
	msignal.Slot.call(this,signal,listener,once,priority);
};
msignal.Slot2.__name__ = true;
msignal.Slot2.__super__ = msignal.Slot;
msignal.Slot2.prototype = $extend(msignal.Slot.prototype,{
	execute: function(value1,value2) {
		if(!this.enabled) {
			return;
		}
		if(this.once) {
			this.remove();
		}
		if(this.param1 != null) {
			value1 = this.param1;
		}
		if(this.param2 != null) {
			value2 = this.param2;
		}
		this.listener(value1,value2);
	}
	,__class__: msignal.Slot2
});
msignal.SlotList = function(head,tail) {
	this.nonEmpty = false;
	if(head == null && tail == null) {
		if(msignal.SlotList.NIL != null) {
			throw new js._Boot.HaxeError("Parameters head and tail are null. Use the NIL element instead.");
		}
		this.nonEmpty = false;
	} else if(head == null) {
		throw new js._Boot.HaxeError("Parameter head cannot be null.");
	} else {
		this.head = head;
		this.tail = tail == null ? msignal.SlotList.NIL : tail;
		this.nonEmpty = true;
	}
};
msignal.SlotList.__name__ = true;
msignal.SlotList.prototype = {
	get_length: function() {
		if(!this.nonEmpty) {
			return 0;
		}
		if(this.tail == msignal.SlotList.NIL) {
			return 1;
		}
		var result = 0;
		var p = this;
		while(p.nonEmpty) {
			++result;
			p = p.tail;
		}
		return result;
	}
	,prepend: function(slot) {
		return new msignal.SlotList(slot,this);
	}
	,insertWithPriority: function(slot) {
		if(!this.nonEmpty) {
			return new msignal.SlotList(slot);
		}
		var priority = slot.priority;
		if(priority >= this.head.priority) {
			return this.prepend(slot);
		}
		var wholeClone = new msignal.SlotList(this.head);
		var subClone = wholeClone;
		var current = this.tail;
		while(current.nonEmpty) {
			if(priority > current.head.priority) {
				subClone.tail = current.prepend(slot);
				return wholeClone;
			}
			subClone = subClone.tail = new msignal.SlotList(current.head);
			current = current.tail;
		}
		subClone.tail = new msignal.SlotList(slot);
		return wholeClone;
	}
	,filterNot: function(listener) {
		if(!this.nonEmpty || listener == null) {
			return this;
		}
		if(Reflect.compareMethods(this.head.listener,listener)) {
			return this.tail;
		}
		var wholeClone = new msignal.SlotList(this.head);
		var subClone = wholeClone;
		var current = this.tail;
		while(current.nonEmpty) {
			if(Reflect.compareMethods(current.head.listener,listener)) {
				subClone.tail = current.tail;
				return wholeClone;
			}
			subClone = subClone.tail = new msignal.SlotList(current.head);
			current = current.tail;
		}
		return this;
	}
	,find: function(listener) {
		if(!this.nonEmpty) {
			return null;
		}
		var p = this;
		while(p.nonEmpty) {
			if(Reflect.compareMethods(p.head.listener,listener)) {
				return p.head;
			}
			p = p.tail;
		}
		return null;
	}
	,__class__: msignal.SlotList
};
var React = require("react");
var ReactDOM = require("react-dom");
var $_, $fid = 0;
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $fid++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; o.hx__closures__[m.__id__] = f; } return f; }
String.prototype.__class__ = String;
String.__name__ = true;
Array.__name__ = true;
msignal.SlotList.NIL = new msignal.SlotList(null,null);
js.Boot.__toStr = ({ }).toString;
mogodev.app.clients.clientReact.view.ApiNode.theme = { scheme : "monokai", author : "wimer hazenberg (http://www.monokai.nl)", base00 : "#272822", base01 : "#383830", base02 : "#49483e", base03 : "#75715e", base04 : "#a59f85", base05 : "#f8f8f2", base06 : "#f5f4f1", base07 : "#f9f8f5", base08 : "#f92672", base09 : "#fd971f", base0A : "#f4bf75", base0B : "#a6e22e", base0C : "#a1efe4", base0D : "#66d9ef", base0E : "#ae81ff", base0F : "#cc6633"};
mogodev.app.clients.clientReact.view.ApiNode.displayName = "ApiNode";
mogodev.app.clients.clientReact.view.SurveyApp.displayName = "SurveyApp";
mogodev.app.clients.clientReact.Main.main();
})(typeof window != "undefined" ? window : typeof global != "undefined" ? global : typeof self != "undefined" ? self : this);

//# sourceMappingURL=client.js.map